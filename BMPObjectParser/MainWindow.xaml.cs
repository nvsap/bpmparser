﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Configuration;
using System.Collections.Specialized;

namespace BMPObjectParser
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Download_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //string DataSource = "WIN - JG56R3MF2FF";
                //string initialCatalog = "bashmag_dev";
                //string userId = "Supervisor";
                //string password = "Supervisor";
                string conn = @"metadata=res://*/DataModel.csdl|res://*/DataModel.ssdl|res://*/DataModel.msl;provider=System.Data.SqlClient;provider connection string=';data source=" +
                               dataSource.Text + ";initial catalog=" + initCatalog.Text + ";persist security info=True;user id=" +
                                login.Text + ";Password=" + password.Text + ";MultipleActiveResultSets=True;App=EntityFramework';";
                updateConfigFile(conn);
                ConfigurationManager.RefreshSection("connectionStrings");
            }
            catch (Exception E)
            {
                MessageBox.Show(ConfigurationManager.ConnectionStrings["con"].ToString() + ".This is invalid connection", "Incorrect server/Database");
            }
            string s = ObjectParcer.CreateExcel();
        }

        public void updateConfigFile(string con)
        {
            //updating config file
            XmlDocument XmlDoc = new XmlDocument();
            //Loading the Config file
            XmlDoc.Load(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
            foreach (XmlElement xElement in XmlDoc.DocumentElement)
            {
                if (xElement.Name == "connectionStrings")
                {
                    //setting the coonection string
                    xElement.FirstChild.Attributes[1].Value = con;
                }
            }
            //writing the connection string in config file
            XmlDoc.Save(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
        }
        
    }
}
