﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace BMPObjectParser
{
    class ObjectParcer
    {
        public static string CreateExcel()
        {
            var excelApp = new Excel.Application();
            excelApp.Workbooks.Add();
            Excel._Worksheet workSheet = (Excel.Worksheet)excelApp.ActiveSheet;

            #region makeExcelHeader

            workSheet.Cells[1, 1] = "Имя схемы";
            workSheet.Cells[1, 2] = "Имя объекта";
            workSheet.Cells[1, 3] = "Тип объекта";
            List<Culture> cultList = new List<Culture>();
            using (dbContext db = new dbContext())
            {
                var culture = db.SysCultures;
                int a = 1;
                foreach (var item in culture)
                {
                    cultList.Add(new Culture() { CultId = a+3, CultGuid = item.Id, CultName = item.Name});
                    workSheet.Cells[1, a + 3] = item.Name;
                    a++;
                }
            }
            #endregion


            #region downloadData
            List<SysSchem> sysSchemaData = new List<SysSchem>();
            List<SysLocalzValue> sysLocalzData = new List<SysLocalzValue>();
            using (dbContext db = new dbContext())
            {
                var sysSch = db.SysSchemas.Where(b => b.ManagerName == "EntitySchemaManager");
                var sysSche = sysSch.Where(b => b.Name != "BaseLookup");
                var sysSchema = sysSche.Where(b => b.Name != "BaseObject");
                var sysLocalz = db.SysLocalizableValues;
                foreach(var item in sysSchema)
                {
                    sysSchemaData.Add(new SysSchem() { SsId = item.Id, SsName = item.Name, SsExtendParent = item.ExtendParent, SsParentId = item.ParentId });
                }
                foreach(var item in sysLocalz.ToList())
                {
                    if (item.Key.StartsWith("Columns"))
                    {
                        string[] spl = item.Key.Split('.');
                        var sql = @"SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + item.SysSchema.Name + "' AND COLUMN_NAME ='" + spl[1] + "'";
                        //var sql = @"SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Contact' AND COLUMN_NAME ='Name'";
                        var getType = db.Database.SqlQuery<string>(sql).ToList();
                        if (getType.Count() > 0)
                        {
                            sysLocalzData.Add(new SysLocalzValue() { SysSchemaId = item.SysSchemaId, SysCultureId = item.SysCultureId, SysLoclzKey = spl[1], SysLoclzValue = item.Value, ColumnType = getType[0] });
                        }
                        else
                        {
                            sysLocalzData.Add(new SysLocalzValue() { SysSchemaId = item.SysSchemaId, SysCultureId = item.SysCultureId, SysLoclzKey = spl[1], SysLoclzValue = item.Value});
                        }
                    }
                }
            }
            #endregion

            #region Structured Data
            List<BpmTable> bpmTable = new List<BpmTable>();
            List<BpmObject> bpmObj = new List<BpmObject>();
            var sysSchemaWithoutParent = sysSchemaData.Where(b => b.SsExtendParent == false);
            foreach(var item in sysSchemaWithoutParent)
            {
                var sysLclz = sysLocalzData.Where(n => n.SysSchemaId == item.SsId);

                foreach (var locl in sysLclz)
                {
                    var cultureList = cultList.Where(b => b.CultGuid == locl.SysCultureId);
                    List<Localz> loc = new List<Localz>();
                    foreach (var i in cultureList)
                    {
                        loc.Add(new Localz() { CultGuid = locl.SysCultureId, Value = locl.SysLoclzValue, LocId = i.CultId });
                    }
                    bpmObj.Add(new BpmObject() { Key = locl.SysLoclzKey, TableId = locl.SysSchemaId, Locl = loc, TableName = item.SsName, ObjectType = locl.ColumnType });
                }
                bpmTable.Add(new BpmTable() { Id = item.SsId, Name = item.SsName});
            }
            var sysSchemaWithParent = sysSchemaData.Where(b => b.SsExtendParent == true);
            foreach (var item in bpmTable)
            {
                var sysSchemaChild = sysSchemaWithParent.Where(b => b.SsName == item.Name);
                if (sysSchemaChild.Count() > 0)
                {
                    foreach (var table in sysSchemaChild)
                    {
                        var sysLclz = sysLocalzData.Where(n => n.SysSchemaId == table.SsId);
                        
                        foreach (var locl in sysLclz)
                        {
                            var bpmObjLclz = bpmObj.Where(n => n.Key == locl.SysLoclzKey);
                            //if (item.SsExtendParent == true)
                            //{
                            if (bpmObjLclz.Count() > 0)
                            {
                                foreach (var c in bpmObjLclz)
                                {
                                    var s = c.Locl.Where(b => b.CultGuid == locl.SysCultureId);
                                    if ((s != null)&&(table.SsParentId == c.TableId))
                                    {
                                        c.Locl.Remove(s.Single());
                                    }
                                    var cultureList = cultList.Where(b => b.CultGuid == locl.SysCultureId);
                                    foreach (var i in cultureList)
                                    {
                                        c.Locl.Add(new Localz() { CultGuid = locl.SysCultureId, Value = locl.SysLoclzValue, LocId = i.CultId});
                                    }
                                    c.ObjectType = locl.ColumnType;
                                }
                            }
                            //}
                            else
                            {
                                var cultureList = cultList.Where(b => b.CultGuid == locl.SysCultureId);
                                List<Localz> loc = new List<Localz>();
                                foreach (var i in cultureList)
                                {
                                    loc.Add(new Localz() { CultGuid = locl.SysCultureId, Value = locl.SysLoclzValue, LocId = i.CultId });
                                }
                                bpmObj.Add(new BpmObject() { Key = locl.SysLoclzKey, TableId = locl.SysSchemaId, Locl = loc, TableName =  table.SsName, ObjectType = locl.ColumnType });
                            }
                        }
                    }
                }
            }

            #endregion

            #region Creating Excel
            int y = 2;
            foreach(var item in bpmTable)
            {
                var tableData = bpmObj.Where(b => b.TableName == item.Name);
                workSheet.Cells[y, 1] = item.Name;
                y++;
                foreach (var data in tableData)
                {
                    workSheet.Cells[y, 2] = data.Key;
                    workSheet.Cells[y, 3] = data.ObjectType;
                    foreach(var loclz in data.Locl)
                    {
                        var cultData = cultList.Single(b => b.CultGuid == loclz.CultGuid);
                        workSheet.Cells[y, cultData.CultId] = loclz.Value;
                    }
                    y++;
                }
                y++;
            }
            #region autofit
            workSheet.Columns[1].AutoFit();
            workSheet.Columns[2].AutoFit();
            workSheet.Columns[3].AutoFit();
            workSheet.Columns[4].AutoFit();
            workSheet.Columns[5].AutoFit();
            workSheet.Columns[6].AutoFit();
            workSheet.Columns[7].AutoFit();
            workSheet.Columns[8].AutoFit();
            #endregion
            excelApp.Visible = true;
            #endregion
            return "Ok";
        }
        class Culture
        {
            public int CultId { get; set; }
            public string CultName { get; set; }
            public Guid CultGuid { get; set; }
        }
        class SysSchem
        {
            public Guid SsId { get; set; }
            public string SsName { get; set; }
            public bool SsExtendParent { get; set; }
            public Guid? SsParentId { get; set; }
        }
        class SysLocalzValue
        {
            public Guid? SysSchemaId { get; set; }
            public Guid? SysCultureId { get; set; }
            public string SysLoclzKey { get; set; }
            public string ColumnType { get; set; }
            public string SysLoclzValue { get; set; }
        }
        class BpmTable
        {
            public Guid? Id { get; set; }
            public string Name { get; set; }
        }
        class BpmObject
        {
            public string Key { get; set; }
            public Guid? TableId { get; set; }
            public string TableName { get; set; }
            public string ObjectType { get; set; }
            public List<Localz> Locl = new List<Localz>();
        }
        class Localz
        {
            public int LocId { get; set; }
            public string Value { get; set; }
            public Guid? CultGuid { get; set; }
        }
    }
}
