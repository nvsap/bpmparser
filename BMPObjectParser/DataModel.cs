
namespace BMPObjectParser

{

    using System;

    using System.Data.Entity;

    using System.Data.Entity.Infrastructure;



    public partial class dbContext : DbContext
    {
        public dbContext()
            : base("name=dbContext")
        {
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }

        public virtual DbSet<SysCulture> SysCultures { get; set; }
        public virtual DbSet<SysLocalizableValue> SysLocalizableValues { get; set; }
        public virtual DbSet<SysSchema> SysSchemas { get; set; }
    }
}
